﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAuthenController : ControllerBase
    {
       
        readonly DBContext _context;
        //public UserAuthenController(DBContext context)
        //{
        //    _context = context;
        //}
        // GET: api/UserAuthen
        [HttpGet]
        public IEnumerable<User> Get()
             
        {
            UserAuthen sv = new UserAuthen(_context);
            var result = sv.GetUserAll();
            return result;
        }

        // GET: api/UserAuthen/5
        [HttpGet("{id}", Name = "GetUser")]
        public string GetUser(int id)
        {
            return "value";
        }

        // POST: api/UserAuthen
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }
    }
}
