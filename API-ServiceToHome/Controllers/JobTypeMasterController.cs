﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobTypeMasterController : ControllerBase
    {
        readonly DBContext _context;
        public JobTypeMasterController(DBContext context)
        {
            _context = context;
        }
        // GET: api/JobTpyeMaster
        [HttpGet]
        public IEnumerable<Job> Get()
        {
            return _context.Job.ToList();
        }
    }
}
