﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleMasterController : ControllerBase
    {
        readonly DBContext _context;
        public RoleMasterController(DBContext context)
        {
            _context = context;
        }
        // GET: api/RoleMaster
        [HttpGet]
        public IEnumerable<Role> Get()
        {
            return _context.Role.ToList();
        }




    }
}
