﻿using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Services
{
    public class UserAuthen
    {
        readonly DBContext _context;
        public UserAuthen(DBContext context)
        {
            _context = context;
        }
        public List<User> GetUserAll()
        {
            try
            {
                //List<User> us = new List<User>();
                return _context.User.ToList();
            }
            catch
            {
                return null;
            }
         
        }
    }
}
