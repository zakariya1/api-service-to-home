﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class Special
    {
        public int SpecialID { get; set; }
        public string SpecialName { get; set; }
        public string SpecialDes { get; set; }
        public bool isActive { get; set; }
        public Nullable<DateTime> Created_at { get; set; }
        public Nullable<DateTime> Updated_at { get; set; }

    }
}
