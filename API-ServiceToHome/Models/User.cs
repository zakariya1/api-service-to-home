﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string TitleTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public string TitleEN { get; set; }
        public string FirstNameEN { get; set; }
        public string LastNameEN { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Line_ID { get; set; }
        public int? RegisType_ID { get; set; }
        public int? Role_ID { get; set; }
        public int? Special_ID { get; set; }
        public string isActive { get; set; }
        public string Token_ID { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<DateTime> Created_at { get; set; }
        public Nullable<DateTime> Updated_at { get; set; }
    }
}
