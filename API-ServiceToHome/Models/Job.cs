﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class Job
    {
        public int JobID { get; set; }
        public string JobName { get; set; }
        public string JobDes { get; set; }
        public string isActive { get; set; }
        public Nullable<DateTime> Created_at { get; set; }
        public Nullable<DateTime> Updated_at { get; set; }

    }
}
